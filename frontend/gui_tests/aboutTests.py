import time
import sys
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys


DRIVER = "./frontend/gui_tests/chromedriver.exe"
PAGE = "/about"


class aboutTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1920,1080")
        cls.driver = webdriver.Chrome(DRIVER, options=options)
        cls.driver.get(URL)
        time.sleep(10)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testTeamCardsAllRender(self):
        teamHeader = self.driver.find_elements_by_tag_name("h1")[1]
        teamTable = teamHeader.find_element_by_xpath("../div")
        members = teamTable.find_elements_by_class_name("col")
        assert len(members) == 5

    def testPositiveCommitNumber(self):
        statsHeader = self.driver.find_elements_by_tag_name("h1")[2]
        repoStats = statsHeader.find_element_by_xpath("..")
        commitsStat = repoStats.find_elements_by_class_name("card-text")[0]
        numCommits = int(commitsStat.text.split()[1])
        assert numCommits > 0

    def testCardLinksOpenNewTab(self):
        apiHeader = self.driver.find_elements_by_tag_name("h1")[3]
        apiSection = apiHeader.find_element_by_xpath("..")
        npsCard = apiSection.find_elements_by_class_name("card-text")[0]
        self.driver.execute_script("arguments[0].scrollIntoView(true);", npsCard)
        time.sleep(3)
        npsCard.click()
        tabs = self.driver.window_handles
        assert len(tabs) == 2

        self.driver.switch_to.window(tabs[1])
        assert (
            self.driver.current_url
            == "https://www.nps.gov/subjects/developer/api-documentation.htm"
        )
        self.driver.close()
        self.driver.switch_to.window(tabs[0])
        self.driver.execute_script("window.scrollTo(0, 0);")
        time.sleep(3)


if __name__ == "__main__":
    DRIVER = sys.argv[1]
    URL = sys.argv[2] + PAGE
    unittest.main(argv=["first-arg-is-ignored"])
