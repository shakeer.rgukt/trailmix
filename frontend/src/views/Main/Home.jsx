import { React, useState } from "react";
import styles from "./Home.module.css";
import { Button, Card, Col, Container, Modal, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const Home = () => {
  //const [state, setState] = useState([]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div className={styles.mainPage}>
      <div className={styles.leftBanner}>
        <Container>
          <Row>
            <Col>
              <h1 className={styles.titleLeft}>TrailMix</h1>
              <p className={styles.tagline}>
                Need a break? Take in the natural world around you and explore a
                national park today!
              </p>
            </Col>
          </Row>
          <Row>
            <Col className={styles.prezButton}>
              <Button variant="primary" onClick={handleShow}>
                Presentation
              </Button>

              <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
              >
                <Modal.Header closeButton>
                  <Modal.Title>Presentation Video</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Row className="video-responsive">
                    <iframe
                      width="465"
                      height="315"
                      src="https://www.youtube.com/embed/MD9cOMNVGnk"
                      title="YouTube video player"
                      frameBorder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                    ></iframe>
                  </Row>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>
            </Col>
          </Row>
        </Container>
      </div>
      <div className={styles.modelsBanner}>
        <h1 className={styles.titleModel}>Models</h1>
        <div className={styles.cardBox}>
          <Link to="/parks" className={styles.linkText}>
            <Card className={styles.parksCard}>
              <Card.Body className={styles.cardBody}>
                <Card.Text className={styles.cardText}>
                  National Parks
                </Card.Text>
              </Card.Body>
            </Card>
          </Link>
          <Link to="/trails" className={styles.linkText}>
            <Card className={styles.trailsCard}>
              <Card.Body className={styles.cardBody}>
                <Card.Text className={styles.cardText}>Hiking Trails</Card.Text>
              </Card.Body>
            </Card>
          </Link>
          <Link to="/lodges" className={styles.linkText}>
            <Card className={styles.lodgeCard}>
              <Card.Body className={styles.cardBody}>
                <Card.Text className={styles.cardText}>Lodging</Card.Text>
              </Card.Body>
            </Card>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Home;
