import alberttrevino from "./img/alberttrevino.jpg";
import johnjin from "./img/johnjin.jpg";
import orionreynolds from "./img/orionreynolds.jpg";
import philipzeng from "./img/philipzeng.jpg";
import toridenney from "./img/toridenney.jpg";

const teamMembers = [
  {
    Name: "Tori Denney",
    Username: "toridenney",
    Email: "tori.denney@att.net",
    Photo: toridenney,
    Role: "Trails Model",
    Bio: "Hi! My name is Tori Denney and I am a senior at the University of Texas at Austin, majoring in Computer Science and minoring in Business. A few of my favorite things include hiking, handstands, photography, traveling, good food and good people.",
    Commits: 0,
    Issues: 0,
    Tests: 0,
  },
  {
    Name: "John Jin",
    Username: "johnjin0000",
    Email: "johnljin000@gmail.com",
    Photo: johnjin,
    Role: "Parks Model",
    Bio: "Hi, I am a junior computer science major at UT Austin. I am from a city in China and I enjoy food, volleyball, and spending time with friends.",
    Commits: 0,
    Issues: 0,
    Tests: 4,
  },
  {
    Name: "Orion Reynolds",
    Username: "orionreynolds",
    Email: "orionreynolds@gmail.com",
    Photo: orionreynolds,
    Role: "Back End | DevOps | Project Leader",
    Bio: "Hello! I'm currently a third-year student at UT Austin majoring in Computer Science and Urban Studies. In my free time, I like to play Overwatch and take friends around Austin via Cap Metro.",
    Commits: 0,
    Issues: 0,
    Tests: 36,
  },
  {
    Name: "Albert Trevino",
    Username: "alberttrevino19",
    Email: "alberttrevino19@gmail.com",
    Photo: alberttrevino,
    Role: "Lodging Model",
    Bio: "Hi, I'm Albert Trevino and I am a fourth year computer science student at UT. I love drumming and also playing Overwatch in my free time when I'm not with friends!",
    Commits: 0,
    Issues: 0,
    Tests: 3,
  },
  {
    Name: "Philip Zeng",
    Username: "zeng-philip",
    Email: "zengphilip0@gmail.com",
    Photo: philipzeng,
    Role: "Back End | Home, About",
    Bio: "I am currently a 3rd year at the University of Texas at Austin. I'm a big basketball fan and enjoy playing video games in my free time.",
    Commits: 0,
    Issues: 0,
    Tests: 34,
  },
];

export { teamMembers };
