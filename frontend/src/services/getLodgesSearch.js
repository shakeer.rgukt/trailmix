async function getLodgesSearch(
  searchTerm,
  stateFilter,
  reservationFilter,
  lodgingFilter,
  feeFilter,
  size,
  currentPage,
  sortKey
) {
  if (feeFilter != "") {
    // must be "eq" or "ne"
    feeFilter = `,{"name":"fee_description","op":"${feeFilter}","val":"No fees specified"}`;
  }
  if (reservationFilter != "") {
    // must be "true" or "false"
    reservationFilter = reservationFilter == "true";
    reservationFilter = `,{"name":"reservable","op":"eq","val":${reservationFilter}}`;
  }
  let filters = `{"and":[{"name":"lodging_type","op":"like","val":"%25${lodgingFilter}%25"},{"name":"state","op":"like","val":"%25${stateFilter}%25"}${reservationFilter}${feeFilter}`;
  searchTerm = searchTerm.replace(" ", "%25");

  filters = filters.concat(`]}`);
  let response = await fetch(
    `https://api.trailmixapp.me/lodging?sort=${sortKey}&page[size]=${size}&page[number]=${currentPage}&filter[objects]=[{"or":[{"name":"name","op":"ilike","val":"%25${searchTerm}%25"},{"name":"state","op":"ilike","val":"%25${searchTerm}%25"},{"name":"location","op":"ilike","val":"%25${searchTerm}%25"},{"name":"lodging_type","op":"ilike","val":"%25${searchTerm}%25"},{"name":"description","op":"ilike","val":"%25${searchTerm}%25"}]},${filters}]`,
    {
      headers: {
        "Content-Type": "application/vnd.api+json",
        Accept: "application/vnd.api+json",
      },
    }
  );
  if (!response.ok) {
    throw new Error("Error: ${response.status}");
  }
  let data = response.json();
  console.log(data);
  return data;
}

export default getLodgesSearch;
