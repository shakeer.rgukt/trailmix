let EPSILON = 0.00001; // for distance

async function getTrailsSearch(
  searchTerm,
  stateFilter,
  reservationFilter,
  feesFilter,
  petsFilter,
  difficultyFilter,
  distanceFilter,
  size,
  currentPage,
  sortKey
) {
  let filters = `{"and":[`;
  let filtersCount = 0;
  searchTerm = searchTerm.replace(" ", "%25");

  if (difficultyFilter != "") {
    filters = filters.concat(
      `{"name":"difficulty","op":"like","val":"%25${difficultyFilter}%25"},`
    );
    filtersCount++;
  }

  if (stateFilter != "") {
    filters = filters.concat(
      `{"name":"states","op":"like","val":"%25${stateFilter}%25"},`
    );
    filtersCount++;
  }

  if (reservationFilter != "") {
    let reserveFilterBoolean = reservationFilter == "true" ? true : false;
    filters = filters.concat(
      `{"name":"reservation","op":"eq","val":${reserveFilterBoolean}},`
    );
    filtersCount++;
  }

  if (feesFilter != "") {
    let feeFilterBoolean = feesFilter == "true" ? true : false;
    filters = filters.concat(
      `{"name":"fees","op":"eq","val":${feeFilterBoolean}},`
    );
    filtersCount++;
  }

  if (petsFilter != "") {
    let petFilterBoolean = petsFilter == "true" ? true : false;
    filters = filters.concat(
      `{"name":"pets","op":"eq","val":${petFilterBoolean}},`
    );
    filtersCount++;
  }

  distanceFilter = parseFloat(distanceFilter);
  if (!isNaN(distanceFilter)) {
    filters = filters.concat(
      `{"name":"distance","op":"le","val":${distanceFilter}},{"name":"distance","op":"ge","val":${EPSILON}},`
    );
    filtersCount++;
  } else if (sortKey.indexOf("distance") > -1) {
    filters = filters.concat(`{"name":"distance","op":"ge","val":${EPSILON}},`);
    filtersCount++;
  }

  if (filtersCount > 0) {
    filters = filters.slice(0, -1);
  }
  filters = filters.concat(`]}`);

  let response = await fetch(
    `https://api.trailmixapp.me/trail?sort=${sortKey}&page[size]=${size}&page[number]=${currentPage}&filter[objects]=[{"or":[{"name":"name","op":"ilike","val":"%25${searchTerm}%25"},{"name":"title","op":"ilike","val":"%25${searchTerm}%25"},{"name":"difficulty","op":"ilike","val":"%25${searchTerm}%25"},{"name":"states","op":"ilike","val":"%25${searchTerm}%25"},{"name":"description","op":"ilike","val":"%25${searchTerm}%25"},{"name":"imageCaption","op":"ilike","val":"%25${searchTerm}%25"}]},${filters}]`,
    {
      headers: {
        "Content-Type": "application/vnd.api+json",
        Accept: "application/vnd.api+json",
      },
    }
  );
  if (!response.ok) {
    throw new Error("Error: ${response.status}");
  }
  let data = response.json();
  console.log(data);
  return data;
}

export default getTrailsSearch;
