async function getNationalParks(size, currentPage) {
  let response = await fetch(
    `https://api.trailmixapp.me/park?page[size]=${size}&page[number]=${currentPage}`,
    {
      headers: { Accept: "application/vnd.api+json" },
    }
  );
  let data = response.json();
  console.log("Success: ", data);
  return data;
}

export default getNationalParks;
