async function getParksSearch(
  searchTerm,
  stateFilter,
  size,
  currentPage,
  sortKey
) {
  searchTerm = searchTerm.replace(" ", "%25");
  let filter = "";
  if (stateFilter != "") {
    filter = `,{"and":[{"name":"states","op":"like","val":"%25${stateFilter}%25"}]}`;
  }
  let response = await fetch(
    `https://api.trailmixapp.me/park?sort=${sortKey}&page[size]=${size}&page[number]=${currentPage}&filter[objects]=[{"or":
    [{"name":"name","op":"ilike","val":"%25${searchTerm}%25"},
    {"name":"parkCode","op":"ilike","val":"%25${searchTerm}%25"},
    {"name":"designation","op":"ilike","val":"%25${searchTerm}%25"},
    {"name":"description","op":"ilike","val":"%25${searchTerm}%25"},
    {"name":"phys_address","op":"ilike","val":"%25${searchTerm}%25"}]} 
    ${filter}]`,
    {
      headers: {
        "Content-Type": "application/vnd.api+json",
        Accept: "application/vnd.api+json",
      },
    }
  );
  if (!response.ok) {
    throw new Error(`Error: ${response.status}`);
  }
  let data = response.json();
  console.log(data);
  return data;
}

export default getParksSearch;
