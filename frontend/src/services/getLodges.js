async function getLodges(size, currentPage) {
  let response = await fetch(
    `https://api.trailmixapp.me/lodging?page[size]=${size}&page[number]=${currentPage}`,
    {
      headers: {
        "Content-Type": "application/vnd.api+json",
        Accept: "application/vnd.api+json",
      },
    }
  );
  if (!response.ok) {
    throw new Error("Error: ${response.status}");
  }
  let data = response.json();
  console.log(data);
  return data;
}

export default getLodges;
