import React from "react";
import "regenerator-runtime/runtime";
import About from "../views/About/About";
import Home from "../views/Main/Home";
import Map from "../components/Map";
import TrailsPage from "../components/TrailsPage";
import Trail from "../components/Trail";
import NationalParksHome from "../components/NationalParksSplash";
import NationalPark from "../components/NationalPark";
import LodgesPage from "../components/LodgesPage";
import { shallow } from "enzyme";
import Lodges from "../components/Lodges";
import SearchPage from "../components/SearchPage";

const mockUseLocationData = {
  pathname: "https://coolwebsite.com",
  data: {
    title: "Hike to Papaya",
    name: "Papaya Trail",
    image: "https://www.coolwebsite.com/papaya.jpg",
    photoCaption: "Papaya Photo",
    relatedPark: "Papaya Park",
    states: "TX",
    desciption: "Papaya",
    activities: ["eat papaya"],
    topics: ["fruit"],
    tags: ["papaya", "hiking"],
    pets: "Yes",
    fees: "Yes",
    reservation: "Yes",
    lat: "0",
    long: "-20",
  },
};
jest.mock("react-router", () => ({
  ...jest.requireActual("react-router"),
  useLocation: jest.fn().mockImplementation(() => {
    return mockUseLocationData;
  }),
}));

describe("Renders page views", () => {
  test("About page", () => {
    const aboutRender = shallow(<About />);
    expect(aboutRender).toMatchSnapshot();
  });

  test("Home page", () => {
    const homeRender = shallow(<Home />);
    expect(homeRender).toMatchSnapshot();
  });

  test("Parks splash page", () => {
    const parkSplashRender = shallow(<NationalParksHome />);
    expect(parkSplashRender).toMatchSnapshot();

    // finding search bar and attribute sorters
    expect(parkSplashRender.find("Form")).toHaveLength(1);
  });

  test("Park instance page", () => {
    const parkRender = shallow(<NationalPark />);
    expect(parkRender).toMatchSnapshot();
  });

  test("Trails splash page", () => {
    const trailSplashRender = shallow(<TrailsPage />);
    expect(trailSplashRender).toMatchSnapshot();

    // finding search bar and attribute sorters
    expect(trailSplashRender.find("Form")).toHaveLength(1);
  });

  test("Trail instance page", () => {
    const trailRender = shallow(<Trail />);
    expect(trailRender).toMatchSnapshot();
  });

  test("Lodge instance page", () => {
    const lodgeRender = shallow(<LodgesPage />);
    expect(lodgeRender).toMatchSnapshot();
  });

  test("Lodge splash page", () => {
    const lodgeSplashRender = shallow(<Lodges />);
    expect(lodgeSplashRender).toMatchSnapshot();

    // finding search bar and attribute sorters
    expect(lodgeSplashRender.find("Form")).toHaveLength(1);
  });

  test("Search page", () => {
    const searchRender = shallow(<SearchPage />);
    expect(searchRender).toMatchSnapshot();

    // finding search bar and attribute sorters
    expect(searchRender.find("Form")).toHaveLength(1);
  });
});

describe("Renders components", () => {
  test("Map component", () => {
    const mapRender = shallow(
      <Map
        latitude={0}
        longitude={0}
        loadingElement={<div style={{ height: `100%` }} />}
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCT5-gsWDCa1DiRcbIhz9x3tveMUmjI3Zc"
      />
    );
    expect(mapRender).toMatchSnapshot();
  });

  test("Map component invalid URL", () => {
    const mapRender = shallow(
      <Map
        latitude={0}
        longitude={0}
        loadingElement={<div style={{ height: `100%` }} />}
        googleMapURL="https://maps.googleapis.com/"
      />
    );
    expect(mapRender).toMatchSnapshot();
  });
});
