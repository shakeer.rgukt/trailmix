import React, { useState, useEffect, useContext } from "react";
import Image from "react-bootstrap/Image";
import Map from "./Map";
import PropTypes from "prop-types";
import Form from "react-bootstrap/Form";
import getNationalPark from "../services/getNationalPark";
import getLodge from "../services/getLodge";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import {
  Accordion,
  AccordionContext,
  Col,
  Container,
  Row,
  ToggleButton,
  useAccordionButton,
} from "react-bootstrap";
import { Link } from "react-router-dom";

const LodgesPage = (props) => {
  const [lodges_info, set_lodges_info] = useState({});
  const [lodges_info_loaded, set_lodges_info_loaded] = useState(false);

  const [park_info, set_park_info] = useState({});
  const [park_info_loaded, set_park_info_loaded] = useState(false);

  useEffect(() => {
    let lodgeName = props.location.pathname.split("/").at(-1);
    lodgeName = lodgeName.replaceAll(" ", "%20");
    getLodge(lodgeName).then((response) => {
      set_lodges_info(response["data"]);
      set_lodges_info_loaded(true);

      getNationalPark(
        response["data"]["relationships"]["park"]["data"]["id"]
      ).then((response) => {
        set_park_info(response["data"]);
        set_park_info_loaded(true);
      });
    });
  }, []);

  const load_park = () => {
    const park_link = "/parks/view/" + park_info["id"];
    return (
      <Row className="Card">
        <Col>
          <h3>Related Park:</h3>
          <hr />
          <h4>
            {park_info_loaded ? (
              <Link to={park_link} className="hover-link">
                {park_info["attributes"]["name"]} -{" "}
                {park_info["attributes"]["states"]}
              </Link>
            ) : (
              // <div
              //   className="hover-link"
              //   onClick={() => {
              //     {
              //       loadParkPage(park_info, park_info["id"]);
              //     }
              //   }}
              // >
              //   {park_info["attributes"]["name"]} -{" "}
              //   {park_info["attributes"]["states"]}
              // </div>
              <FontAwesomeIcon icon={faSpinner} spin />
            )}
          </h4>
        </Col>
      </Row>
    );
  };

  const load_trails = () => {
    return (
      <Row className="Card">
        <Col>
          <h3> Nearby Trails: </h3>
          <hr />

          {lodges_info["relationships"]["trails"]["data"].length != 0 ? (
            lodges_info["relationships"]["trails"]["data"].map(
              (trail, index) => {
                const trail_link = "/trails/view/" + trail["id"];
                return (
                  <React.Fragment key={index}>
                    <h4>
                      <Link className="hover-link" to={trail_link}>
                        {trail["id"]}
                      </Link>
                    </h4>
                  </React.Fragment>
                );
              }
            )
          ) : (
            <h4>No Nearby Trails</h4>
          )}
        </Col>
      </Row>
    );
  };

  function ToggleImage({ eventKey, callback }) {
    /* Adapted from: https://react-bootstrap.github.io/components/accordion/ */
    const { activeEventKey } = useContext(AccordionContext);

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    );

    const isCurrentEventKey = activeEventKey == eventKey;

    return (
      <ToggleButton
        className="mb-2"
        id="tb-image"
        variant={isCurrentEventKey ? "outline-primary" : "primary"}
        onClick={decoratedOnClick}
      >
        {isCurrentEventKey ? "Hide Image" : "Show Image"}
      </ToggleButton>
    );
  }

  const load_image = () => {
    let no_img = String(lodges_info["attributes"]["image"]).length == 0;

    return (
      <Row className="Card">
        <Col>
          {no_img ? (
            <h4>No image available</h4>
          ) : (
            <Accordion defaultActiveKey="0">
              <ToggleImage eventKey="0">Show Image</ToggleImage>
              <Accordion.Collapse eventKey="0">
                <Image src={lodges_info["attributes"]["image"]} fluid />
              </Accordion.Collapse>
            </Accordion>
          )}
          <p>
            {no_img ? (
              <></>
            ) : (
              <Form.Label className="PhotoCaption">
                {lodges_info["attributes"]["imageCaption"]}
              </Form.Label>
            )}
          </p>
        </Col>
      </Row>
    );
  };

  const load_address = () => {
    let addr1 = lodges_info["attributes"]["address1"];
    let addr2 = lodges_info["attributes"]["address2"];
    let no_address = String(addr1).length + String(addr2).length == 0;
    return (
      <Row className="Card">
        <Col>
          <h3>Address:</h3>
          <hr />
          <h4>
            {no_address ? (
              "No address available (see related park)"
            ) : (
              <>
                {addr1}
                <br />
                {addr2}
              </>
            )}
          </h4>
        </Col>
      </Row>
    );
  };

  const load_description = () => {
    return (
      <Row className="Card">
        <Col>
          <h3>Description:</h3>
          <hr />
          <h4
            dangerouslySetInnerHTML={{
              __html: lodges_info["attributes"]["description"],
            }}
          />
        </Col>
      </Row>
    );
  };

  const load_contact = () => {
    return (
      <Row className="Card">
        <Col>
          <h3>Contact Info:</h3>
          <hr />
          <h4>Email: {lodges_info["attributes"]["email"]}</h4>
          <h4>Phone Number: {lodges_info["attributes"]["phone"]}</h4>
        </Col>
      </Row>
    );
  };

  const load_fee = () => {
    return (
      <Row className="Card">
        <Col>
          <h3>Fee:</h3>
          <hr />
          <h4
            dangerouslySetInnerHTML={{
              __html: lodges_info["attributes"]["fee_description"],
            }}
          />
        </Col>
      </Row>
    );
  };

  const load_activities = () => {
    return (
      <Row className="Card">
        <Col>
          <h3>Activities:</h3>
          <hr />
          {lodges_info["attributes"]["activities"].map((activity, index) => {
            return <h4 key={index}>{activity}</h4>;
          })}
        </Col>
      </Row>
    );
  };

  return (
    <React.Fragment>
      <div className="Container">
        {lodges_info_loaded ? (
          <Container className="Container">
            <Row className="Card">
              <Col>
                <h1>{lodges_info["attributes"]["name"]}</h1>
                <hr />
                <h2>{lodges_info["attributes"]["lodging_type"]}</h2>
              </Col>
            </Row>
            {load_image()}
            {load_description()}
            {load_address()}
            {load_park()}
            {load_trails()}
            {load_contact()}
            {load_fee()}
            {load_activities()}
            <Map
              latitude={parseFloat(lodges_info["attributes"]["lat"])}
              longitude={parseFloat(lodges_info["attributes"]["long"])}
              loadingElement={<div style={{ height: `100%` }} />}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCT5-gsWDCa1DiRcbIhz9x3tveMUmjI3Zc"
              containerElement={<div style={{ height: `400px` }} />}
              mapElement={<div style={{ height: `100%` }} />}
            />
          </Container>
        ) : (
          <h1>
            <FontAwesomeIcon
              icon={faSpinner}
              spin
              style={{ margin: "auto", display: "block" }}
            />
          </h1>
        )}
      </div>
    </React.Fragment>
  );
};

LodgesPage.propTypes = {
  location: PropTypes.object,
  history: PropTypes.object,
  children: PropTypes.element,
  eventKey: PropTypes.string,
  callback: PropTypes.object,
};

export default LodgesPage;
