import React, { useState, useEffect } from "react";
import {
  Table,
  Pagination,
  Row,
  Col,
  Form,
  Button,
  FloatingLabel,
  Container,
} from "react-bootstrap";
import getLodges from "../services/getLodges";
import PropTypes from "prop-types";
import getLodgesSearch from "../services/getLodgesSearch";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faSpinner } from "@fortawesome/free-solid-svg-icons";

const Lodges = (props) => {
  const lodges_per_page = 20;
  const states = [
    "Alabama - AL",
    "Alaska - AK",
    "Arizona - AZ",
    "Arkansas - AR",
    "California - CA",
    "Colorado - CO",
    "Connecticut - CT",
    "Delaware - DE",
    "Florida - FL",
    "Georgia - GA",
    "Hawaii - HI",
    "Idaho - ID",
    "Illinois - IL",
    "Indiana - IN",
    "Iowa - IA",
    "Kansas - KS",
    "Kentucky - KY",
    "Louisiana - LA",
    "Maine - ME",
    "Maryland - MD",
    "Massachusetts - MA",
    "Michigan - MI",
    "Minnesota - MN",
    "Mississippi - MS",
    "Missouri - MO",
    "Montana - MT",
    "Nebraska - NE",
    "Nevada - NV",
    "New Hampshire - NH",
    "New Jersey - NJ",
    "New Mexico - NM",
    "New York - NY",
    "North Carolina - NC",
    "North Dakota - ND",
    "Ohio - OH",
    "Oklahoma - OK",
    "Oregon - OR",
    "Pennsylvania - PA",
    "Rhode Island - RI",
    "South Carolina - SC",
    "South Dakota - SD",
    "Tennessee - TN",
    "Texas - TX",
    "Utah - UT",
    "Vermont - VT",
    "Virginia - VA",
    "Washington - WA",
    "West Virginia - WV",
    "Wisconsin - WI",
    "Wyoming - WY",
  ];

  const [lodges, set_lodges] = useState({});
  const [lodges_loaded, set_lodges_loaded] = useState(false);
  const [total_lodges_count, set_total_lodges_count] = useState(1);
  const [total_page_count, set_total_page_count] = useState(1);
  let [current_page, set_current_page] = useState(1);

  const [searchTerm, setSearchTerm] = useState("");
  const [stateFilter, setStateFilter] = useState("");
  const [reservationFilter, setReservationFilter] = useState("");
  const [feeFilter, setFeeFilter] = useState("");

  const [lodgingFilter, setLodgingFilter] = useState("");

  const [sortKey, setSortKey] = useState("name");

  const [sitewideSearchTermUsed, setSitewideSearchTermUsed] = useState(false);

  useEffect(() => {
    if (props.location.state == null) {
      get_lodges_at_page(1);
    } else {
      setSearchTerm(props.location.state.sitewideSearchTerm);
      setSitewideSearchTermUsed(true);
    }
  }, []);

  // to make sitewide searching work
  useEffect(() => {
    if (sitewideSearchTermUsed) {
      search(1);
    }
  }, [sitewideSearchTermUsed]);

  const load_lodge_page = (data, id) => {
    props.history.push({
      pathname: `/lodges/view/${id}`,
      state: { data: data },
    });
  };

  const get_lodges_at_page = (currentPage) => {
    getLodges(lodges_per_page, currentPage, sortKey)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        set_lodges(response);
        set_lodges_loaded(true);

        if (currentPage == 1) {
          updatePagination(response);
        }
      });
  };

  const load_new_lodge_page = (currentPage) => {
    getFilteredLodges(currentPage);
  };

  const reset = () => {
    setSortKey("name");
    setSearchTerm("");
    setStateFilter("");
    setReservationFilter("");
    setLodgingFilter("");
    setFeeFilter("");

    set_lodges_loaded(false);
  };

  const changeSearchTerm = (event) => {
    const search = event.target.value;
    setSearchTerm(search);
  };

  const search = (currentPage) => {
    if (isNaN(currentPage)) {
      currentPage = 1;
    }

    if (searchTerm.length > 0) {
      // getSearchedLodges(currentPage);
      getFilteredLodges(currentPage);
    }
  };

  // const getSearchedLodges = (currentPage) => {
  //   set_lodges_loaded(false);

  //   getLodgesSearchNoFilter(searchTerm, lodges_per_page, currentPage, sortKey)
  //     .catch((error) => {
  //       console.error("Error:", error);
  //     })
  //     .then((response) => {
  //       set_lodges(response);
  //       set_lodges_loaded(true);

  //       if (currentPage == 1) {
  //         updatePagination(response);
  //       }
  //     });
  // };

  const getFilteredLodges = (currentPage) => {
    set_lodges_loaded(false);

    const state = stateFilter.length > 0 ? stateFilter.split(" - ")[1] : "";

    getLodgesSearch(
      searchTerm,
      state,
      reservationFilter,
      lodgingFilter,
      feeFilter,
      lodges_per_page,
      currentPage,
      sortKey
    )
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        set_lodges(response);
        set_lodges_loaded(true);

        if (currentPage == 1) {
          updatePagination(response);
        }
      });
  };

  const filter = () => {
    getFilteredLodges(1);
  };

  const updateStateFilter = (event) => {
    const stateFilter = event.target.value;
    setStateFilter(stateFilter);
  };

  const updateReservationFilter = (event) => {
    const reservationFilter = event.target.value;
    setReservationFilter(reservationFilter);
  };

  const updateLodgingFilter = (event) => {
    const difficultyFilter = event.target.value;
    setLodgingFilter(difficultyFilter);
  };

  const updateFeeFilter = (event) => {
    const feeFilter = event.target.value;
    setFeeFilter(feeFilter);
  };

  const updateSortKey = (event) => {
    const sortKey = event.target.value;
    setSortKey(sortKey);
  };

  const updatePagination = (response) => {
    if (response && response["meta"]) {
      // error checking data was returned
      set_total_lodges_count(response["meta"]["total"]);
      set_total_page_count(
        response["meta"]["total"] % lodges_per_page == 0
          ? Math.floor(response["meta"]["total"] / lodges_per_page)
          : Math.floor(response["meta"]["total"] / lodges_per_page) + 1
      );
      set_current_page(1);
    }
  };

  const goToPage = (event) => {
    current_page = event.target.text;
    set_current_page(current_page);
    set_lodges_loaded(false);

    load_new_lodge_page(current_page);
  };

  const nextPage = () => {
    current_page = parseInt(current_page) + 1;
    set_current_page(current_page);
    set_lodges_loaded(false);

    load_new_lodge_page(current_page);
  };

  const lastPage = () => {
    current_page = total_page_count;
    set_current_page(current_page);
    set_lodges_loaded(false);

    load_new_lodge_page(current_page);
  };

  const backPage = () => {
    current_page = parseInt(current_page) - 1;
    set_current_page(current_page);
    set_lodges_loaded(false);

    load_new_lodge_page(current_page);
  };

  const firstPage = () => {
    current_page = 1;
    set_current_page(current_page);
    set_lodges_loaded(false);

    load_new_lodge_page(current_page);
  };

  const pagination = () => {
    if (lodges_loaded) {
      return (
        <div className="grid-footer">
          <Pagination className="float-right">
            {current_page != 1 && (
              <Pagination.First onClick={() => firstPage()} />
            )}
            {current_page != 1 && (
              <Pagination.Prev onClick={() => backPage()} />
            )}

            {current_page == 1 && <Pagination.First disabled />}
            {current_page == 1 && <Pagination.Prev disabled />}

            {current_page != 1 && (
              <Pagination.Item onClick={(event) => goToPage(event)}>
                {parseInt(current_page) - 1}
              </Pagination.Item>
            )}
            <Pagination.Item className="pagination activeCell">
              {current_page}
            </Pagination.Item>

            {current_page != total_page_count && (
              <Pagination.Item onClick={(event) => goToPage(event)}>
                {parseInt(current_page) + 1}
              </Pagination.Item>
            )}

            {current_page == total_page_count && <Pagination.Next disabled />}
            {current_page == total_page_count && <Pagination.Last disabled />}

            {current_page != total_page_count &&
              parseInt(current_page) + 1 != total_page_count && (
                <Pagination.Ellipsis disabled />
              )}
            {current_page != total_page_count &&
              parseInt(current_page) + 1 != total_page_count && (
                <Pagination.Item onClick={(event) => goToPage(event)}>
                  {total_page_count}
                </Pagination.Item>
              )}

            {current_page != total_page_count && (
              <Pagination.Next onClick={() => nextPage()} />
            )}
            {current_page != total_page_count && (
              <Pagination.Last onClick={() => lastPage()} />
            )}
          </Pagination>
          <h5>
            <strong>{total_lodges_count} RESULTS</strong>
          </h5>
        </div>
      );
    }
  };

  const searchHighlight = (attribute) => {
    if (
      searchTerm.length > 0 &&
      attribute.toUpperCase().includes(searchTerm.toUpperCase())
    ) {
      let searchTermLength = searchTerm.length;
      let attributeString = attribute;
      let attributeArr = [];

      while (attributeString.length > 0) {
        let index = attributeString
          .toUpperCase()
          .indexOf(searchTerm.toUpperCase());

        if (index != -1) {
          let nextString = attributeString.substring(0, index);
          attributeArr.push(nextString);
          let endOfSearchIndex = index + searchTermLength;
          attributeArr.push(attributeString.substring(index, endOfSearchIndex));
          attributeString = attributeString.substring(endOfSearchIndex);
        } else {
          attributeArr.push(attributeString);
          attributeString = "";
        }
      }

      return (
        <div className="d-inline">
          {attributeArr.map((attr) => {
            if (attr.toUpperCase() == searchTerm.toUpperCase()) {
              return (
                <div className="d-inline" style={{ background: "#95e3d3" }}>
                  {attr}
                </div>
              );
            } else {
              return <div className="d-inline">{attr}</div>;
            }
          })}
        </div>
      );
    } else {
      return <div className="d-inline">{attribute}</div>;
    }
  };

  return (
    <div className="Container">
      <Container>
        <h1>Lodging</h1>
        <hr className="dark-line" />
        <Form>
          <Form.Group className="mb-3">
            <Row>
              <Col>
                <Form.Control
                  type="text"
                  placeholder="Enter query here..."
                  className="d-inline"
                  value={searchTerm}
                  onChange={(e) => {
                    changeSearchTerm(e);
                  }}
                />
                <Button
                  variant="secondary"
                  className="d-inline"
                  onClick={search}
                  style={{ marginLeft: "10px" }}
                >
                  Search{" "}
                  <FontAwesomeIcon icon={faSearch} size="1x" color="white" />
                </Button>
              </Col>
            </Row>
          </Form.Group>

          <Form.Group className="mb-3">
            <Row>
              <Col md={3}>
                <FloatingLabel
                  label="State"
                  style={{ color: "black", marginRight: "10px" }}
                >
                  <Form.Select
                    value={stateFilter}
                    onChange={(e) => {
                      updateStateFilter(e);
                    }}
                  >
                    <option value="">Choose...</option>
                    {states.map((state, index) => {
                      return (
                        <option key={index} value={state}>
                          {state}
                        </option>
                      );
                    })}
                  </Form.Select>
                </FloatingLabel>
              </Col>
              <Col md={6} style={{ marginLeft: "10px", marginBottom: "10px" }}>
                <Form.Group>
                  <Form.Label style={{ width: "80px" }}>Reserve?</Form.Label>
                  <Form.Check
                    inline
                    type="radio"
                    label="Yes"
                    checked={reservationFilter == "true"}
                    onChange={updateReservationFilter}
                    style={{ minWidth: "0px" }}
                    name="reserveGroup"
                    value="true"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="No"
                    checked={reservationFilter == "false"}
                    onChange={updateReservationFilter}
                    style={{ minWidth: "0px" }}
                    name="reserveGroup"
                    value="false"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="N/A"
                    checked={reservationFilter == ""}
                    onChange={updateReservationFilter}
                    style={{ minWidth: "0px" }}
                    name="reserveGroup"
                    value=""
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label style={{ width: "80px" }}>Fees?</Form.Label>
                  <Form.Check
                    inline
                    type="radio"
                    label="Yes"
                    checked={feeFilter == "ne"}
                    onChange={updateFeeFilter}
                    style={{ minWidth: "0px" }}
                    value="ne"
                    name="feesGroup"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="No"
                    checked={feeFilter == "eq"}
                    onChange={updateFeeFilter}
                    style={{ minWidth: "0px" }}
                    value="eq"
                    name="feesGroup"
                  />
                  <Form.Check
                    inline
                    type="radio"
                    label="N/A"
                    checked={feeFilter == ""}
                    onChange={updateFeeFilter}
                    style={{ minWidth: "0px" }}
                    value=""
                    name="feesGroup"
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col md={3}>
                <FloatingLabel
                  label="Lodging Type"
                  style={{
                    color: "black",
                    marginRight: "10px",
                    width: "150px",
                  }}
                >
                  <Form.Select
                    value={lodgingFilter}
                    onChange={(e) => {
                      updateLodgingFilter(e);
                    }}
                  >
                    <option value="">Choose...</option>
                    <option value="Campground">Campground</option>
                    <option value="Facility">Facility</option>
                  </Form.Select>
                </FloatingLabel>
              </Col>
              <Col>
                <FloatingLabel
                  label="Sort by"
                  style={{
                    color: "black",
                    marginRight: "10px",
                    width: "150px",
                  }}
                >
                  <Form.Select
                    value={sortKey}
                    onChange={(e) => {
                      updateSortKey(e);
                    }}
                  >
                    <option value="name">Name (A-Z)</option>
                    <option value="-name">Name (Z-A)</option>
                    <option value="lodging_type,name">Type (Asc.)</option>
                    <option value="-lodging_type,name">Type (Desc.)</option>
                  </Form.Select>
                </FloatingLabel>
              </Col>
            </Row>
            <Row>
              <Col xl={3}>
                <Button
                  variant="secondary"
                  style={{
                    width: "170px",
                    marginLeft: "10px",
                    marginBottom: "10px",
                  }}
                  onClick={filter}
                >
                  Filter & Sort
                </Button>
                <Button
                  variant="secondary"
                  style={{ marginLeft: "10px", marginBottom: "10px" }}
                  onClick={() => {
                    reset();
                    get_lodges_at_page(1);
                  }}
                >
                  Reset
                </Button>
              </Col>
            </Row>
          </Form.Group>
        </Form>

        {lodges_loaded && lodges && lodges["data"] ? (
          <Table
            variant="light"
            responsive
            striped
            bordered
            hover
            className="lodges-table"
          >
            <thead>
              <tr>
                <th>Name</th>
                <th>City</th>
                <th>State</th>
                <th>Fees</th>
                <th>Lodging Type</th>
                <th>Reservable</th>
              </tr>
            </thead>
            <tbody>
              {lodges["data"].map((lodge, index) => {
                return (
                  <tr
                    key={index}
                    onClick={() => {
                      load_lodge_page(lodge, lodge["id"]);
                    }}
                  >
                    <td>
                      {
                        <strong>
                          {searchHighlight(lodge["attributes"]["name"])}
                        </strong>
                      }
                    </td>
                    <td>{searchHighlight(lodge["attributes"]["location"])}</td>

                    <td>{searchHighlight(lodge["attributes"]["state"])}</td>
                    <td>
                      {searchHighlight(
                        lodge["attributes"]["fee_description"] !=
                          "No fees specified"
                          ? "Yes"
                          : "No"
                      )}
                    </td>
                    <td>
                      {searchHighlight(lodge["attributes"]["lodging_type"])}
                    </td>
                    <td>{String(lodge["attributes"]["reservable"])}</td>
                  </tr>
                );
              })}{" "}
            </tbody>
          </Table>
        ) : (
          <h2>
            <FontAwesomeIcon icon={faSpinner} spin />
          </h2>
        )}

        {pagination()}
      </Container>
    </div>
  );
};

Lodges.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
};

export default Lodges;
