import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import {
  Container,
  Image,
  Col,
  Row,
  Accordion,
  AccordionContext,
  useAccordionButton,
  ToggleButton,
} from "react-bootstrap";
import Map from "./Map";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

import getNationalPark from "../services/getNationalPark";
import getLodge from "../services/getLodge";

const NationalPark = (props) => {
  const [parkInfo, setParkInfo] = useState({});
  const [parkInfoLoaded, setParkInfoLoaded] = useState(false);
  const [lodging, setLodging] = useState({});
  const [lodgingLoaded, setLodgingLoaded] = useState(false);

  useEffect(() => {
    const park_id = props.location.pathname.split("/").at(-1);
    getNationalPark(park_id)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setParkInfo(response["data"]);
        setParkInfoLoaded(true);
        // grab relationship lodging
        let arrayOfPromises = [];
        for (let lodge of response["data"]["relationships"]["lodging"][
          "data"
        ]) {
          arrayOfPromises.push(
            getLodge(lodge["id"])
              .catch((error) => {
                console.error("Error:", error);
              })
              .then((response) => {
                return response;
              })
          );
        }
        Promise.all(arrayOfPromises).then((result) => {
          setLodging(result);
          setLodgingLoaded(true);
        });
      });
  }, []);

  const loadPhysicalAddress = () => {
    return (
      <div>
        <h3>Address:</h3>
        <hr />
        <h4>{parkInfo["attributes"]["phys_address"]}</h4>
      </div>
    );
  };

  function ToggleImage({ eventKey, callback }) {
    /* Adapted from: https://react-bootstrap.github.io/components/accordion/ */
    const { activeEventKey } = useContext(AccordionContext);

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    );

    const isCurrentEventKey = activeEventKey == eventKey;

    return (
      <ToggleButton
        className="mb-2"
        id="tb-image"
        variant={isCurrentEventKey ? "outline-primary" : "primary"}
        onClick={decoratedOnClick}
      >
        {isCurrentEventKey ? "Hide Image" : "Show Image"}
      </ToggleButton>
    );
  }

  const loadImage = () => {
    return (
      <Accordion defaultActiveKey="0">
        <ToggleImage eventKey="0">Show Image</ToggleImage>
        <Accordion.Collapse eventKey="0">
          <Image src={parkInfo["attributes"]["image"]} fluid />
        </Accordion.Collapse>
      </Accordion>
    );
  };

  const loadDescription = () => {
    return (
      <div>
        <h3>Description:</h3>
        <hr />
        <h4>{parkInfo["attributes"]["description"]}</h4>
      </div>
    );
  };

  const loadContactInfo = () => {
    return (
      <div>
        <h3>Contact Info:</h3>
        <hr />
        <h4>Email: {parkInfo["attributes"]["email"]}</h4>
        <h4>Phone: {parkInfo["attributes"]["phone"]}</h4>
      </div>
    );
  };

  const loadActivities = () => {
    return (
      <div>
        <h3>Activities:</h3>
        <hr />
        {parkInfo["attributes"]["activities"].map((activity, index) => {
          return <h4 key={index}>{activity}</h4>;
        })}
      </div>
    );
  };

  const loadLodging = () => {
    return (
      <div>
        <h3> Nearby Lodging: </h3>
        <hr />
        {parkInfo["relationships"]["lodging"]["data"].length != 0 ? (
          lodgingLoaded ? (
            lodging.map((lodge, index) => {
              const lodge_link = "/lodges/view/" + lodge["data"]["id"];
              return (
                <React.Fragment key={index}>
                  <h4>
                    <Link to={lodge_link} key={index} className="hover-link">
                      {lodge["data"]["attributes"]["name"]}
                    </Link>
                  </h4>
                </React.Fragment>
              );
            })
          ) : (
            <FontAwesomeIcon icon={faSpinner} spin />
          )
        ) : (
          <h4>No Nearby Lodging</h4>
        )}
      </div>
    );
  };

  const loadTrails = () => {
    return (
      <div>
        <h3> Nearby Trails: </h3>
        <hr />
        {parkInfo["relationships"]["trails"]["data"].length != 0 ? (
          parkInfo["relationships"]["trails"]["data"].map((trail, index) => {
            const trail_link = "/trails/view/" + trail["id"];
            return (
              <React.Fragment key={index}>
                <h4>
                  <Link to={trail_link} key={index} className="hover-link">
                    {trail["id"]}
                  </Link>
                </h4>
              </React.Fragment>
            );
          })
        ) : (
          <h4>No Nearby Trails</h4>
        )}
      </div>
    );
  };

  const loadEntryFee = () => {
    return (
      <div>
        <h3>Entry Fees:</h3>
        <hr />
        {Object.keys(parkInfo["attributes"]["fee_description"]).map(
          (entranceFee, index) => {
            return (
              <div key={index}>
                <h4>
                  {parkInfo["attributes"]["fee_description"][entranceFee]}
                </h4>
                <h4>{"$" + Number(entranceFee).toFixed(2)}</h4>
                <br />
              </div>
            );
          }
        )}
      </div>
    );
  };

  return (
    <React.Fragment>
      <div className="Container">
        {parkInfoLoaded ? (
          <Container className="Container">
            <Row className="Card">
              <Col>
                <h1>{parkInfo["attributes"]["name"]}</h1>
                <hr />
                <h2>{parkInfo["attributes"]["designation"]}</h2>
              </Col>
            </Row>
            <Row className="Card">
              <Col>{loadImage()}</Col>
            </Row>
            <Row className="Card">
              <Col>{loadPhysicalAddress()}</Col>
            </Row>
            <Row className="Card">
              <Col>{loadLodging()}</Col>
            </Row>
            <Row className="Card">
              <Col>
                <h2>Map:</h2>
                <hr />
                <Map
                  latitude={parkInfo["attributes"]["lat"]}
                  longitude={parkInfo["attributes"]["long"]}
                  loadingElement={<div style={{ height: `100%` }} />}
                  googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCT5-gsWDCa1DiRcbIhz9x3tveMUmjI3Zc"
                  containerElement={<div style={{ height: `400px` }} />}
                  mapElement={<div style={{ height: `100%` }} />}
                />
              </Col>
            </Row>
            <Row className="Card">
              <Col>{loadContactInfo()}</Col>
            </Row>
            <Row className="Card">
              <Col>{loadDescription()}</Col>
            </Row>
            <Row className="Card">
              <Col>{loadEntryFee()}</Col>
            </Row>
            <Row className="Card">
              <Col>{loadActivities()}</Col>
            </Row>
            <Row className="Card">
              <Col>{loadTrails()}</Col>
            </Row>
            {/* Maybe in the future add carousel of images */}
          </Container>
        ) : (
          <h1>
            <FontAwesomeIcon
              icon={faSpinner}
              spin
              style={{ margin: "auto", display: "block" }}
            />
          </h1>
        )}
      </div>
    </React.Fragment>
  );
};

NationalPark.propTypes = {
  location: PropTypes.object,
  children: PropTypes.element,
  eventKey: PropTypes.string,
  callback: PropTypes.object,
};

export default NationalPark;
