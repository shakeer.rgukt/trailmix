import React, { useState, useEffect, useContext } from "react";
import {
  Form,
  Image,
  ListGroup,
  Row,
  Col,
  Container,
  Table,
} from "react-bootstrap";
import { useLocation } from "react-router";
import {
  Badge,
  Accordion,
  AccordionContext,
  useAccordionButton,
  ToggleButton,
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";
import Map from "./Map";
import ".././App.css";
import getTrail from "../services/getTrail";
import getNationalPark from "../services/getNationalPark";
import getLodge from "../services/getLodge";

const Trail = (props) => {
  const locationInfo = useLocation();

  const [trailInfo, setTrailInfo] = useState({});
  const [trailInfoLoaded, setTrailInfoLoaded] = useState(false);

  const [nationalPark, setNationalPark] = useState({});
  const [nationalParkLoaded, setNationalParkLoaded] = useState(false);

  const [lodging, setLodging] = useState({});
  const [lodgingLoaded, setLodgingLoaded] = useState(false);

  useEffect(() => {
    let trailName = props.location.pathname.split("/").at(-1);
    trailName = trailName.replaceAll(" ", "%20");
    getTrail(trailName)
      .catch((error) => {
        console.error("Error:", error);
      })
      .then((response) => {
        setTrailInfo(response["data"]);
        setTrailInfoLoaded(true);

        // grab relationship park
        getNationalPark(response["data"]["relationships"]["park"]["data"]["id"])
          .catch((error) => {
            console.error("Error:", error);
          })
          .then((response) => {
            setNationalPark(response["data"]);
            setNationalParkLoaded(true);
          });

        // grab relationship lodging
        let arrayOfPromises = [];
        for (let lodge of response["data"]["relationships"]["lodging"][
          "data"
        ]) {
          arrayOfPromises.push(
            getLodge(lodge["id"])
              .catch((error) => {
                console.error("Error:", error);
              })
              .then((response) => {
                return response;
              })
          );
        }
        Promise.all(arrayOfPromises).then((result) => {
          setLodging(result);
          setLodgingLoaded(true);
        });
      });
  }, [locationInfo]);

  function htmlDecode(input) {
    let doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
  }

  function ToggleImage({ eventKey, callback }) {
    const { activeEventKey } = useContext(AccordionContext);

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    );

    const isCurrentEventKey = activeEventKey == eventKey;

    return (
      <ToggleButton
        className="mb-2"
        id="tb-image"
        variant={isCurrentEventKey ? "outline-primary" : "primary"}
        onClick={decoratedOnClick}
      >
        {isCurrentEventKey ? "Hide Image" : "Show Image"}
      </ToggleButton>
    );
  }

  const loadParkPage = (data, id) => {
    props.history.push({
      pathname: `/parks/view/${id}`,
      state: { data: data },
    });
  };

  const loadLodgePage = (data, id) => {
    props.history.push({
      pathname: `/lodges/view/${id}`,
      state: { data: data },
    });
  };

  return (
    <div className="Container">
      {trailInfoLoaded ? (
        <Container className="Container">
          <ListGroup>
            <ListGroup.Item className="Card">
              <Form>
                <br />
                <h1 className="Title">{trailInfo["attributes"]["title"]}</h1>
                <hr />
                <h2 className="Title">{trailInfo["attributes"]["name"]}</h2>

                <Form.Group className="mb-3">
                  <Accordion defaultActiveKey="0">
                    <ToggleImage eventKey="0">Show Image</ToggleImage>
                    <Accordion.Collapse eventKey="0">
                      <Image src={trailInfo["attributes"]["image"]} fluid />
                    </Accordion.Collapse>
                  </Accordion>
                </Form.Group>

                <Form.Group className="mb-4">
                  <Form.Label className="PhotoCaption">
                    {trailInfo["attributes"]["photoCaption"]}
                    <br></br>
                    {nationalParkLoaded ? (
                      <div
                        className="hover-link"
                        onClick={() => {
                          {
                            loadParkPage(nationalPark, nationalPark["id"]);
                          }
                        }}
                      >
                        {nationalPark["attributes"]["name"]} -{" "}
                        {nationalPark["attributes"]["states"]}
                      </div>
                    ) : (
                      <h4>
                        <FontAwesomeIcon icon={faSpinner} spin />
                      </h4>
                    )}
                  </Form.Label>
                </Form.Group>

                <Form.Group className="mb-4">
                  <Form.Label className="mb-3">
                    {htmlDecode(trailInfo["attributes"]["description"])}
                  </Form.Label>
                </Form.Group>

                <Container className="mb-4">
                  <Row>
                    <Col sm={8}>
                      <Form.Group>
                        <Form.Label className="mb-3">
                          <strong>Nearby Lodging:</strong>
                          &nbsp;
                          {lodgingLoaded ? (
                            lodging.map((lodge, index) => {
                              return (
                                <h5
                                  key={index}
                                  onClick={() => {
                                    {
                                      loadLodgePage(
                                        lodge["data"],
                                        lodge["data"]["id"]
                                      );
                                    }
                                  }}
                                >
                                  {" "}
                                  <Badge
                                    pill
                                    bg="light"
                                    style={{ color: "#1abc9c" }}
                                    className="hover-pill"
                                  >
                                    {lodge["data"]["attributes"]["name"]}
                                  </Badge>{" "}
                                </h5>
                              );
                            })
                          ) : (
                            <FontAwesomeIcon icon={faSpinner} spin />
                          )}
                        </Form.Label>
                      </Form.Group>

                      <Form.Group>
                        <Form.Label className="mb-3">
                          <strong>Activities:</strong>
                          &nbsp;
                          {trailInfo["attributes"]["activities"].map(
                            (activity, index) => {
                              return (
                                <h5 key={index}>
                                  {" "}
                                  <Badge pill bg="primary">
                                    {activity}
                                  </Badge>{" "}
                                </h5>
                              );
                            }
                          )}
                        </Form.Label>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label className="mb-3">
                          <strong>Topics:</strong>
                          &nbsp;
                          {trailInfo["attributes"]["topics"].map(
                            (topic, index) => {
                              return (
                                <h5 key={index}>
                                  {" "}
                                  <Badge pill bg="secondary">
                                    {topic}
                                  </Badge>{" "}
                                </h5>
                              );
                            }
                          )}
                        </Form.Label>
                      </Form.Group>
                      <Form.Group>
                        <Form.Label>
                          <strong>Tags:</strong>
                          &nbsp;
                          {trailInfo["attributes"]["tags"].map((tag, index) => {
                            return (
                              <h5 key={index}>
                                {" "}
                                <Badge pill bg="success">
                                  {tag}
                                </Badge>{" "}
                              </h5>
                            );
                          })}
                        </Form.Label>
                      </Form.Group>
                    </Col>
                    <Col sm={4}>
                      <Table variant="light" responsive striped bordered hover>
                        <tbody>
                          <tr>
                            <td>
                              <strong>Reservation required?</strong>
                            </td>
                            <td>
                              {trailInfo["attributes"]["reservation"]
                                ? "Yes"
                                : "No"}
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <strong>Fees?</strong>
                            </td>
                            <td>
                              {trailInfo["attributes"]["fees"] ? "Yes" : "No"}
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <strong>Pets Allowed?</strong>
                            </td>
                            <td>
                              {trailInfo["attributes"]["pets"] ? "Yes" : "No"}
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </Container>

                <Form.Group className="mb-2">
                  {trailInfo["attributes"]["lat"] &&
                    trailInfo["attributes"]["long"] && (
                      <Map
                        latitude={parseFloat(trailInfo["attributes"]["lat"])}
                        longitude={parseFloat(trailInfo["attributes"]["long"])}
                        loadingElement={<div style={{ height: `100%` }} />}
                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCT5-gsWDCa1DiRcbIhz9x3tveMUmjI3Zc"
                        containerElement={<div style={{ height: `400px` }} />}
                        mapElement={<div style={{ height: `100%` }} />}
                      />
                    )}
                </Form.Group>
              </Form>
            </ListGroup.Item>
          </ListGroup>
        </Container>
      ) : (
        <h1>
          <FontAwesomeIcon
            icon={faSpinner}
            spin
            style={{ margin: "auto", display: "block" }}
          />
        </h1>
      )}
    </div>
  );
};

Trail.propTypes = {
  location: PropTypes.object,
  children: PropTypes.element,
  eventKey: PropTypes.string,
  callback: PropTypes.object,
  history: PropTypes.object,
};

export default Trail;
