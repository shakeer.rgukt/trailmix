import React, { useState } from "react";
import TrailsChart from "../visualizations/TrailsChart";
import LodgesChart from "../visualizations/LodgesChart";
import ParksChart from "../visualizations/ParksChart";
import NutripalCalorieChart from "../visualizations/NutripalCalorieChart";
import NutripalRecipeCategoryChart from "../visualizations/NutripalRecipeCategoryChart";
import NutripalScatterChart from "../visualizations/NutripalScatterChart";
import { Tab, Nav, Card, Container } from "react-bootstrap";
// source: https://react-bootstrap.netlify.app/components/tabs/

const Visualizations = () => {
  const [title, setTitle] = useState("TrailMix");

  return (
    <div className="Container">
      <Container className="Container">
        <h1 style={{ textAlign: "center" }}>{title} Visualizations</h1>
        <Tab.Container defaultActiveKey="trailmix">
          <Nav
            variant="tabs"
            className="viz-bar"
            style={{
              marginBottom: "-5px",
              flex: "1 1 auto",
              display: "flex",
              flexDirection: "column",
              flexWrap: "wrap",
              alignContent: "end",
            }}
          >
            <Nav.Item>
              <Nav.Link
                onClick={() => {
                  setTitle("MyNutriPal");
                }}
                eventKey="nutripal"
              >
                Provider
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                onClick={() => {
                  setTitle("TrailMix");
                }}
                eventKey="trailmix"
              >
                Us
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <Tab.Content>
            <Tab.Pane eventKey="trailmix">
              <Card
                style={{ color: "gray", paddingTop: "1em" }}
                className="pb-4 pt-4 mb-4"
              >
                <TrailsChart />
              </Card>
              <Card
                style={{ color: "gray", paddingTop: "1em" }}
                className="pb-4 pt-4 mb-4"
              >
                <ParksChart />
              </Card>
              <Card
                style={{ color: "gray", paddingTop: "1em" }}
                className="pb-4 pt-4 mb-4"
              >
                <LodgesChart />
              </Card>
            </Tab.Pane>
            <Tab.Pane eventKey="nutripal">
              <Card style={{ color: "gray" }} className="pb-4 pt-4 mb-4">
                <NutripalCalorieChart />
              </Card>
              <Card style={{ color: "gray" }} className="pb-4 pt-4 mb-4">
                <NutripalRecipeCategoryChart />
              </Card>
              <Card style={{ color: "gray" }} className="pb-4 pt-4 mb-4">
                <NutripalScatterChart />
              </Card>
            </Tab.Pane>
          </Tab.Content>
        </Tab.Container>
      </Container>
    </div>
  );
};

export default Visualizations;
