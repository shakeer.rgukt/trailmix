import React, { useState, useEffect } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Label,
  ResponsiveContainer,
} from "recharts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const getNutritionInfo = async () => {
  let nutritionInfo = [];
  let dataList = [];
  let dataLog = [];
  let pageNum = 0;
  do {
    nutritionInfo = await fetch(
      `https://r1muet4wvk.execute-api.us-east-2.amazonaws.com/dev/nutritionalInfo?page=${pageNum}`
    );
    nutritionInfo = await nutritionInfo.json();

    dataLog = JSON.parse(nutritionInfo);

    dataLog.data.forEach((item) => {
      dataList = [...dataList, item.nutrient_energy];
    });

    pageNum += 1;
  } while (dataLog.data.length === 10);
  return dataList;
};

const NutripalCalorieChart = () => {
  const [calories, setCalories] = useState([]);
  const [graphLoaded, setGraphLoaded] = useState(false);

  useEffect(() => {
    const fetchInfo = async () => {
      if (calories === undefined || calories.length === 0) {
        const info = await getNutritionInfo();

        const data = [
          {
            name: "0-49",
            instances: 0,
          },
          {
            name: "50-99",
            instances: 0,
          },
          {
            name: "100- 149",
            instances: 0,
          },
          {
            name: "150- 199",
            instances: 0,
          },
          {
            name: "200-249",
            instances: 0,
          },
          {
            name: "250-299",
            instances: 0,
          },
          {
            name: "300+",
            instances: 0,
          },
        ];

        info.forEach((calorieCount) => {
          let classifier = parseInt(calorieCount / 50);
          if (classifier < 6) {
            data[classifier].instances += 1;
          } else {
            data[6].instances += 1;
          }
        });

        setCalories(data);
      }
    };
    fetchInfo();
    setGraphLoaded(true);
  }, [calories]);

  return (
    <div style={{ textAlign: "center" }}>
      <h1>Nutrition Data</h1>
      <h5>Frequency of Calorie Counts</h5>
      <ResponsiveContainer width="100%" height={500}>
        {graphLoaded && calories.length > 0 ? (
          <BarChart data={calories}>
            <XAxis dataKey="name" height={50}>
              <Label value="Calorie Ranges" dy={20} />
            </XAxis>
            <YAxis dataKey="instances">
              <Label value="Instances" angle={-90} dx={-20} />
            </YAxis>
            <Tooltip />
            <Bar dataKey="instances" fill="#95e3d3" />
          </BarChart>
        ) : (
          <div>
            <FontAwesomeIcon icon={faSpinner} size="2x" spin />
          </div>
        )}
      </ResponsiveContainer>
    </div>
  );
};

export default NutripalCalorieChart;
