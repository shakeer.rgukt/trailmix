import { React, useEffect, useState } from "react";
import getTrails from "../services/getTrails";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Label,
  ResponsiveContainer,
} from "recharts";

const TrailsChart = () => {
  const [trails, setTrails] = useState([]);
  const [trailsLoaded, setTrailsLoaded] = useState(false);
  const [bubbleChartData, setBubbleChartData] = useState([]);
  const trailsPerPage = 500;

  useEffect(() => {
    getAllTrails();
  }, []);

  const getAllTrails = async () => {
    let promises = [];

    let trails = [];

    promises.push(
      getTrails(trailsPerPage, 1)
        .catch((error) => {
          console.error("Error:", error);
        })
        .then((response) => {
          response["data"].forEach((trail) => {
            var trailAttribute = trail["attributes"];
            trails.push(trailAttribute);
          });
          return trails;
        })
    );

    Promise.all(promises).then((values) => {
      let allTrails = [];
      values.forEach((value) => {
        value.forEach((trail) => {
          allTrails.push(trail);
        });
      });

      allTrails.forEach((trail) => {
        let found = {};
        let state = trail["states"];
        if (state.includes(",")) {
          let statesArr = state.split(",");
          statesArr.forEach((state) => {
            found = bubbleChartData.find((e) => e.label == state);
            if (found) {
              let totalDistance = found.value + trail["distance"];
              totalDistance = Math.round(totalDistance * 100) / 100;
              bubbleChartData[bubbleChartData.indexOf(found)].value =
                totalDistance;
            } else if (trail["distance"] != 0) {
              let color = "#";
              let dataPoint = {
                label: state,
                value: Math.round(trail["distance"] * 100) / 100,
                color: color.concat(
                  Math.floor(Math.random() * 16777215).toString(16)
                ),
              };
              bubbleChartData.push(dataPoint);
            }
          });
        } else {
          found = bubbleChartData.find((e) => e.label == state);
          if (found) {
            let totalDistance = found.value + trail["distance"];
            totalDistance = Math.round(totalDistance * 100) / 100;
            bubbleChartData[bubbleChartData.indexOf(found)].value =
              totalDistance;
          } else if (trail["distance"] != 0) {
            let dataPoint = {
              label: state,
              value: Math.round(trail["distance"] * 100) / 100,
            };
            bubbleChartData.push(dataPoint);
          }
        }
      });

      bubbleChartData.sort(function (a, b) {
        return b.value - a.value;
      });
      setBubbleChartData(bubbleChartData);

      setTrails(allTrails);
      setTrailsLoaded(true);
    });
  };

  const trailChartComponent = () => {
    if (trailsLoaded) {
      return (
        <ResponsiveContainer width="100%" height={500}>
          <BarChart
            data={bubbleChartData}
            layout="vertical"
            margin={{ top: 5, right: 30, left: 40, bottom: 20 }}
          >
            <XAxis type="number" dataKey="value">
              <Label value="Total Trail Miles" dy={20} />
            </XAxis>
            <YAxis type="category" dataKey="label">
              <Label value="State" angle={-90} dx={-40} />
            </YAxis>
            <Tooltip />
            <Bar dataKey="value" fill="#95e3d3" barSize={70} barGap={0} />
          </BarChart>
        </ResponsiveContainer>
      );
    }
  };

  return (
    <div style={{ textAlign: "center" }}>
      <h1>Trails Data</h1>
      <h5>Top Mileage of Trails Available By State</h5>
      {trailsLoaded && trails.length > 0 && <div>{trailChartComponent()}</div>}
      {(!trailsLoaded || trails.length == 0) && (
        <div>
          <FontAwesomeIcon icon={faSpinner} size="2x" spin />
        </div>
      )}
    </div>
  );
};

export default TrailsChart;
