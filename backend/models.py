# pylint: disable = too-few-public-methods

"""
TrailMix models
"""

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import ARRAY, JSON
from sqlalchemy.ext.mutable import MutableDict

db: SQLAlchemy = SQLAlchemy()

# many-to-many connection table for Lodging:Trails
lodgtrails = db.Table(
    "lodgtrails",
    db.Column("lodging_id", db.Integer, db.ForeignKey("lodging.id")),
    db.Column("trail_name", db.Unicode, db.ForeignKey("trail.name")),
)


class Park(db.Model):
    """
    National Park Database Model
    One-to-Many with Lodging
    One-to-Many with Trails
    """

    __tablename__ = "park"
    id = db.Column(db.Integer, primary_key=True)
    parkCode = db.Column(db.Unicode, nullable=False)
    name = db.Column(db.Unicode, nullable=False)
    designation = db.Column(db.Unicode, nullable=False)
    max_fee = db.Column(db.Float, nullable=False)
    activities_cnt = db.Column(db.Integer, nullable=False)
    states = db.Column(db.Unicode, nullable=False)
    lat = db.Column(db.Float, nullable=False)
    long = db.Column(db.Float, nullable=False)
    image = db.Column(db.Unicode, nullable=True)
    imageCaption = db.Column(db.Unicode, nullable=True)
    phys_address = db.Column(db.Unicode, nullable=True)
    mail_address = db.Column(db.Unicode, nullable=True)
    email = db.Column(db.Unicode, nullable=True)
    phone = db.Column(db.Unicode, nullable=True)
    description = db.Column(db.Unicode, nullable=False)
    fee_description = db.Column(MutableDict.as_mutable(JSON), nullable=True)
    activities = db.Column(ARRAY(db.Unicode), nullable=True)

    lodging = db.relationship("Lodging", backref="park", lazy="subquery")
    trails = db.relationship("Trail", backref="park", lazy="subquery")

    def __repr__(self):
        return f"<Park {self.id}: {self.name}>"

    def __eq__(self, rhs):
        return isinstance(rhs, Park) and (
            self.id == rhs.id
            and self.parkCode == rhs.parkCode
            and self.name == rhs.name
            and self.designation == rhs.designation
            and self.states == rhs.states
            and self.max_fee == rhs.max_fee
            and self.activities_cnt == rhs.activities_cnt
            and self.lat == rhs.lat
            and self.long == rhs.long
            and self.image == rhs.image
            and self.imageCaption == rhs.imageCaption
            and self.phys_address == rhs.phys_address
            and self.mail_address == rhs.mail_address
            and self.email == rhs.email
            and self.phone == rhs.phone
            and self.description == rhs.description
            and self.fee_description == rhs.fee_description
            and self.activities == rhs.activities
            and self.lodging == rhs.lodging
            and self.trails == rhs.trails
        )


class Trail(db.Model):
    """
    Trail Database Model
    Many-to-One with Parks
    Many-to-Many with Lodging
    """

    __tablename__ = "trail"
    name = db.Column(db.Unicode, primary_key=True)
    title = db.Column(db.Unicode, nullable=False)
    image = db.Column(db.Unicode)
    imageCaption = db.Column(db.Unicode, nullable=True)
    states = db.Column(db.Unicode, nullable=False)
    lat = db.Column(db.Float, nullable=False)
    long = db.Column(db.Float, nullable=False)
    difficulty = db.Column(db.Unicode, nullable=True)
    distance = db.Column(db.Float, nullable=True)
    pets = db.Column(db.Boolean, nullable=False)
    fees = db.Column(db.Boolean, nullable=False)
    reservation = db.Column(db.Boolean, nullable=False)
    activities = db.Column(ARRAY(db.Unicode), nullable=True)
    topics = db.Column(ARRAY(db.Unicode), nullable=True)
    tags = db.Column(ARRAY(db.Unicode), nullable=True)
    description = db.Column(db.Unicode, nullable=False)

    # this is automatically populated as Lodging objects are added to a Park instance
    # note that we only have one for park_id because this is a many-to-one relationship
    park_id = db.Column(db.Integer, db.ForeignKey("park.id"), nullable=False)
    lodging = db.relationship(
        "Lodging",
        secondary=lodgtrails,
        lazy="subquery",
        backref=db.backref("trails", lazy="subquery"),
    )
    # because of the backref, you can also do my_trail.park to get the associated park
    # and thanks to this relationship, my_trail.lodging gets the associated lodging

    def __repr__(self):
        return f"<Trail: {self.name}>"

    def __eq__(self, rhs):
        return isinstance(rhs, Trail) and (
            self.name == rhs.name
            and self.title == rhs.title
            and self.image == rhs.image
            and self.imageCaption == rhs.imageCaption
            and self.states == rhs.states
            and self.lat == rhs.lat
            and self.long == rhs.long
            and self.difficulty == rhs.difficulty
            and self.distance == rhs.distance
            and self.pets == rhs.pets
            and self.fees == rhs.fees
            and self.tags == rhs.tags
            and self.reservation == rhs.reservation
            and self.activities == rhs.activities
            and self.topics == rhs.topics
            and self.description == rhs.description
            and self.park_id == rhs.park_id
            and self.lodging == rhs.lodging
        )


class Lodging(db.Model):
    """
    Lodging Database Model
    Many-to-One with Parks
    Many-to-Many with Trails
    """

    __tablename__ = "lodging"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    location = db.Column(db.Unicode, nullable=False)
    state = db.Column(db.Unicode, nullable=False)
    lodging_type = db.Column(db.Unicode, nullable=False)
    image = db.Column(db.Unicode, nullable=True)
    imageCaption = db.Column(db.Unicode, nullable=True)
    address1 = db.Column(db.Unicode, nullable=True)
    address2 = db.Column(db.Unicode, nullable=True)
    lat = db.Column(db.Float, nullable=False)
    long = db.Column(db.Float, nullable=False)
    email = db.Column(db.Unicode, nullable=True)
    phone = db.Column(db.Unicode, nullable=True)
    fee_description = db.Column(db.Unicode, nullable=False)
    reservable = db.Column(db.Boolean, nullable=False)
    activities = db.Column(ARRAY(db.Unicode), nullable=True)
    description = db.Column(db.Unicode, nullable=False)

    # this is automatically populated as Lodging objects are added to a Park instance
    # note that we only have one for park_id because this is a many-to-one relationship
    park_id = db.Column(db.Integer, db.ForeignKey("park.id"), nullable=False)
    # because of the backref, you can also do my_lodging.park to get the associated park
    # or my_lodging.trails to get the associated trails

    def __eq__(self, rhs):
        return isinstance(rhs, Lodging) and (
            self.name == rhs.name
            and self.id == rhs.id
            and self.location == rhs.location
            and self.state == rhs.state
            and self.lodging_type == rhs.lodging_type
            and self.image == rhs.image
            and self.imageCaption == rhs.imageCaption
            and self.address1 == rhs.address1
            and self.address2 == rhs.address2
            and self.lat == rhs.lat
            and self.long == rhs.long
            and self.email == rhs.email
            and self.phone == rhs.phone
            and self.fee_description == rhs.fee_description
            and self.reservable == rhs.reservable
            and self.activities == rhs.activities
            and self.description == rhs.description
            and self.park_id == rhs.park_id
            and self.trails == rhs.trails
        )

    def __repr__(self):
        return f"<Lodging {self.id}: {self.name}>"
