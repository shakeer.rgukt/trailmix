"""
Flask Application
"""
import os
import logging
from dotenv import load_dotenv
from sqlalchemy.exc import SQLAlchemyError
from flask_restless import APIManager
from flask_cors import CORS  # type: ignore
import util
import populate_db
from models import Trail, Park, Lodging, db

LOAD_ENV_RES = load_dotenv()  # get secrets ;)

# setup logging
LOG_DIR = str(os.getenv("LOG_DIR", default="stderr"))
logger = logging.getLogger(__name__)
util.setup_logging(logger, LOG_DIR, "server", logging.DEBUG)

logger.log(
    logging.INFO if LOAD_ENV_RES else logging.ERROR,
    ".env file %sfound",
    "" if LOAD_ENV_RES else "not ",
)

# Create Flask application
app = util.create_app(db, str(os.getenv("AWS_DB_KEY")))
app.app_context().push()
CORS(app)
logger.info("Flask app context created")
logger.debug("Debug logging on")

# setup SQLAlchemy logging
logs = ["engine", "pool"]
for log in logs:
    sqlalch_logger = logging.getLogger("sqlachlemy." + log)
    util.setup_logging(sqlalch_logger, LOG_DIR, "sqlalch_" + log, logging.DEBUG)
    sqlalch_logger.debug("Debug logging on")

# determine whether or not to (re)populate DB
# be super careful about this, esp. with NPS API query limits
REPOPULATE = bool(os.getenv("TM_REPOPULATE_DB", default=""))
if REPOPULATE:
    # reset the database tables
    db.session.remove()
    db.drop_all()
    logger.warning("Dropped old database")

db.create_all()
logger.info("Database schema created")

# Populate DB
if REPOPULATE:
    logger.warning("Repopulating database")
    NPS_KEY = str(os.getenv("NPS_API_KEY"))
    MAPS_KEY = str(os.getenv("MAPS_API_KEY"))
    RIDB_KEY = str(os.getenv("RIDB_API_KEY"))
    park_list = populate_db.get_parks(logger, NPS_KEY, MAPS_KEY)
    logger.info("Got parks")
    lodging_list = populate_db.get_lodging(
        logger, RIDB_KEY, park_list, radius=20, limit=40
    )
    logger.info("Got lodging")
    trail_list = populate_db.get_trails(logger, NPS_KEY, MAPS_KEY, park_list, limit=30)
    logger.info("Got trails")
    populate_db.link_lt(logger, lodging_list, trail_list, radius=18)
    logger.info("Linked")

    # Add to DB session
    db.session.add_all(park_list)
    logger.info("Parks added to session")
    db.session.add_all(lodging_list)
    logger.info("Lodging added to session")
    db.session.add_all(trail_list)
    logger.info("Trails added to session")

    # Commit to remote DB
    db.session.commit()
    logger.info("Committed to database")

# create splash page for API
app.add_url_rule(
    "/",
    "index",
    lambda: """<html>\n<head> <title>TrailMix API</title> </head>\n
<body><h1>TrailMix API</h1> <p>Welcome to the TrailMix API! View documentation
<a href="https://documenter.getpostman.com/view/17711210/UUy4cR54" target="_blank">here</a>.</p>\n
</body></html>""",
)
logger.info("Splash page created")

# Create the Flask-Restless API manager.
manager = APIManager(app, session=db.session)
logger.info("Flask-Restless AppManager created")

# Create API endpoints, which will be available at /<tablename>.
# Only allow GET HTTP method and set pagination to 20 items per page.
# Flask-Restless is really cool and does filtering, sorting, pagination all for us
# and it only takes one line per model to get a fully JSON-compliant API :)
manager.create_api(
    Trail, methods=["GET"], page_size=20, max_page_size=500, url_prefix="/"
)
logger.info("Trails endpoints created")
manager.create_api(
    Park, methods=["GET"], page_size=20, max_page_size=500, url_prefix="/"
)
logger.info("Parks endpoints created")
manager.create_api(
    Lodging, methods=["GET"], page_size=20, max_page_size=500, url_prefix="/"
)
logger.info("Lodging endpoints created")

# Create rollback handler in case of errors
@app.errorhandler(SQLAlchemyError)
def rollback_on_exception(err):
    """
    Roll back a connection due to a SQLAlchemy exception
    Helpful when connection aborted due to some other issue
    """
    logger.error(err)
    db.session.rollback()  # pylint: disable=no-member


logger.info("Error handler created")

logger.info("Done!")
