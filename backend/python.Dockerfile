FROM python:3.10-slim

COPY requirements.txt /setup/
WORKDIR /setup

RUN apt-get update
RUN apt upgrade -y
RUN apt-get install make -y

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN flask --version

ENV FLASK_APP="server.py"
EXPOSE 5000
CMD "bash"
